#include <array>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <vector>

// https://en.wikipedia.org/wiki/C%2B%2B20
const size_t inset = 1;
const size_t X = 70u - 2 * inset;
const size_t Y = 40u;
using agent_t = bool;
using world_t = std::array<std::array<agent_t, Y + 2 * inset>, X + 2 * inset>;
world_t world;

std::string print_world(const world_t &w, const size_t i) {

  std::stringstream out;

  // Top border
  const std::string border(X + 2, '_');
  out << border << " " << i << "\n";

  // World
  for (size_t y = 0; y < Y; ++y) {
    out << " ";

    for (size_t x = 0; x < X; ++x) {
      const size_t agent = w[x][y];
      out << (agent ? "O" : " ");
    }

    out << " \n";
  }

  return out.str();
}

size_t count_neighbours(const world_t &w, const size_t x, const size_t y) {

  uint32_t sum{0};

  // Count neighbours
  for (size_t i = 0; i < 3; ++i)
    for (size_t j = 0; j < 3; ++j)
      sum += w[x + i - 1][y + j - 1];

  // Deduct selt
  // return sum > 0 ? sum - 1 : 0;
  return sum;
}

world_t test(world_t &now) {

  // Create a copy of the world as it stands
  auto next{now};

  // 1. Any live cell with two or three live neighbours survives.
  // 2. Any dead cell with three live neighbours becomes a live cell.
  // 3. All other live cells die in the next generation. Similarly, all other
  // dead cells stay dead.

  // iterate over each agent in the now world and update the next world
  const size_t x_start = inset;
  const size_t y_start = inset;
  const size_t x_end = X + inset;
  const size_t y_end = Y + inset;

  for (size_t x = x_start; x < x_end; ++x) {
    for (size_t y = y_start; y < y_end; ++y) {

      const size_t n = count_neighbours(now, x, y);

      if (now[x][y] == 0) {
        if (n == 4)
          next[x][y] = true;
      } else {
        if (n == 3 || n == 4) {
		// Survives
        } else if (n < 3)
          next[x][y] = false;
        else
          next[x][y] = false;
      }
    }
  }

  return next;
}

void init() {
  std::random_device rd;
  std::uniform_int_distribution<int> dist(0, 1);

  const size_t x_start = inset;
  const size_t y_start = inset;
  const size_t x_end = X + inset;
  const size_t y_end = Y + inset;

  for (size_t x = x_start; x < x_end; ++x)
    for (size_t y = y_start; y < y_end; ++y)
      world[x][y] = (dist(rd) == 1 ? true : false);
}

int main() {

  // Initialise world
  init();

  std::cout << print_world(world, 0);

  for (size_t i = 0; i < 20; ++i) {

    const auto w{test(world)};

    std::cout << print_world(w, i);

    world = w;
  }
}
